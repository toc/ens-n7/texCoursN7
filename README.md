# Template latex cours N7

Pour utiliser ce template, il suffit de cloner le projet puis d'ajouter dans son `.bashrc` la ligne
suivante :

```bash
export TEXINPUTS=".:$PATH_TO_TEMPLATE/texCoursN7/::"
```

où `PATH_TO_TEMPLATE` est le chemin où se trouve le template.
